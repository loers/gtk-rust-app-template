use glib::StaticType;

mod card;
pub use card::*;

mod nav;
pub use nav::*;

// You need to call static_type() for all widgets you want to use in xml files. Otherwise GTK does not know about them.
pub fn init() {
    card::Card::static_type();
}
