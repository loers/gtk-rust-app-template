// SPDX-License-Identifier: GPL-3.0-or-later

#[macro_use]
extern crate gtk_rust_app;
#[macro_use]
extern crate log;

use gettextrs::gettext;
use gtk::prelude::*;
use gtk_rust_app::widgets::LeafletLayout;
use libadwaita as adw;

use store::{
    gstore::{dispatch, select},
    // this block is required whenever you want to use dispatch or select
    store,
    State,
};

use crate::{pages::Home, widgets::NavHeaderButtons};

// This module will contain our home page
mod pages;
mod store;
mod widgets;

fn main() {
    env_logger::init();

    info!("{}", gettext("Check po/ dir for translations."));

    store::init_store(
        crate::store::State::default(),
        store::reduce,
        vec![crate::store::logging::ExampleLoggingMiddleware::new()],
    );

    // call app builder with metadata from your Cargo.toml and the gresource file compiled by the `gtk_rust_app::build` script (see below).
    gtk_rust_app::app(
        include_bytes!("../Cargo.toml"),
        include_bytes!("../App.toml"),
        include_bytes!("../target/gra-gen/compiled.gresource"),
        None,
    )
    // Connect the global store to gtk action handling
    .store(store::store())
    // include your style sheets here
    .styles(include_str!("styles.css"))
    .build(
        |application, _project_descriptor, settings| {
            // setup custom types
            widgets::init();

            // The pages will be placed in this predefined adaptive layout.
            let leaflet_layout = LeafletLayout::builder(settings)
                .add_page(Home::new())
                .add_main_header_start(NavHeaderButtons::new())
                .build();

            // bind the toast overlay inside the leaflet_layout to the global store notification field.
            handle_notification(&leaflet_layout);

            // and we use the leaflet layout as root content in the apps window.
            let window = gtk_rust_app::window(
                application,
                gettext("Example"),
                settings,
                leaflet_layout.upcast_ref(),
            );
            window.show();
        },
        |app, _project_descriptor, _settings| {
            if let Some(action) = app.lookup_action("quit") {
                let simple_action: gdk4::gio::SimpleAction = action.downcast().unwrap();
                simple_action.connect_activate(glib::clone!(@weak app => move |_, _| {
                    app.quit();
                }));
            }
        },
    );
}

// You can define gobjects like this
#[gobject(id, name)]
pub struct Pair {
    id: String,
    name: String,
}

impl Pair {
    pub fn new(id: &str, name: String) -> Self {
        Self {
            id: id.into(),
            name,
        }
    }
}

fn handle_notification(leaflet_layout: &LeafletLayout) {
    // This selector will be called whenever the state of state.notification changes.
    // Using select! and dispatch! requires the `use store::{gstore::{dispatch, select}, store, State };` in your imports.
    select!(|state| state.notification => glib::clone!(@weak leaflet_layout => move |state| {
        if let Some(notification) = &state.notification {
            let toast = adw::Toast::new(&notification);
            toast.connect_dismissed(|_| {
                dispatch!(crate::store::_CLEAR_NOTIFICATION);
            });
            leaflet_layout.show_toast(&toast);
        }
    }));
}
