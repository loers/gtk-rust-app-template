use crate::store::{gstore::*, store, State};
use gdk4::subclass::prelude::ObjectSubclassIsExt;
use gtk::prelude::*;

// This is a 'smart' widget. It has access to the global store and is therefore more entangled with the overall application.
#[widget(@store extends gtk::Box)]
#[template(file = "nav.ui")]
pub struct NavHeaderButtons {
    #[template_child]
    pub prev_button: TemplateChild<gtk::Button>,
    #[template_child]
    pub next_button: TemplateChild<gtk::Button>,

    #[selector(state.mobile)]
    select_is_mobile: (),
}

impl NavHeaderButtons {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create NavHeaderButtons")
    }

    pub fn constructed(&self) {
        let prev_button = &self.imp().prev_button;
        let next_button = &self.imp().next_button;

        prev_button.connect_clicked(|_b| {
            dispatch!(crate::store::PREV, ("something", false));
        });
        next_button.connect_clicked(|_b| {
            dispatch!(crate::store::NEXT, ("something else", true));
        });
    }

    fn select_is_mobile(&self, state: &State) {
        let mobile = state.mobile;
        self.next_button().set_visible(!mobile);
        self.prev_button().set_visible(!mobile);
    }
}

impl Default for NavHeaderButtons {
    fn default() -> Self {
        Self::new()
    }
}
