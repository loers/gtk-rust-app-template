use gtk_rust_app::gstore;

// You can define a middlewares to dispatch actions on actions or handle any kind of side effect.
#[derive(Default)]
pub struct ExampleLoggingMiddleware;
impl ExampleLoggingMiddleware {
    pub(crate) fn new() -> Box<Self> {
        Box::new(ExampleLoggingMiddleware::default())
    }
}
impl gstore::Middleware<crate::store::State> for ExampleLoggingMiddleware {
    fn pre_reduce(&self, action: &gstore::Action, _state: &crate::store::State) {
        println!("Handling action: {}", action.name())
    }

    fn post_reduce(&self, _action: &gstore::Action, state: &crate::store::State) {
        println!("The state is now : {:#?}", state)
    }
}
