pub use gtk_rust_app::gstore;

use super::State;

// Manipulate the state based on actions.
// You must not dispatch other actions in a reducer. That will lead to infinite loops and break things.
// You can use middlewares to do that.
pub fn reduce(action: &gstore::Action, state: &mut State) {
    match action.name() {
        crate::store::PREV => {
            // get the action arguments
            let (arg1, arg2): (String, bool) = action.arg().unwrap();
            println!("got action args: {} {}", arg1, arg2);
            state.history.pop();
            state.notification = None
        }
        crate::store::NEXT => {
            // get the action arguments
            let (arg1, arg2): (String, bool) = action.arg().unwrap();
            println!("got action args: {} {}", arg1, arg2);
            state.history.push(arg1.to_string());
            state.notification = Some(format!("Go to {}", arg1));
        }
        _ => {
            // ignore
        }
    }
}
