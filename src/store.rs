pub use gtk_rust_app::gstore;

mod history;
pub mod logging;

include!("../target/gra-gen/actions.rs");

// You can have 'private' actions (they will not be called from GTK elements directly).
// These action name should start with a dot.
pub const _CLEAR_NOTIFICATION: &str = ".clear-notification";

#[derive(Debug, Default, Clone, PartialEq, Eq)]
pub struct State {
    pub history: Vec<String>,
    pub notification: Option<String>,
    pub mobile: bool,
}

gstore::store!(State);

pub fn reduce(action: &gstore::Action, state: &mut State) {
    history::reduce(action, state);
}
