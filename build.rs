pub fn main() {
    // Note: There is a bug where projects alwas recompile with this build script. Feel free to comment the lines below when
    // your code generation is not required on every build.
    // You can always trigger it manually using `cargo gra gen`.
    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed=Cargo.toml");
    println!("cargo:rerun-if-changed=src");
    println!("cargo:rerun-if-changed=assets");
    println!("cargo:rerun-if-changed=po");
    gra::build(None, None);
}
